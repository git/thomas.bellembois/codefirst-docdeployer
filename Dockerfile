FROM debian:bookworm
LABEL author="Thomas Bellembois"

RUN apt update && apt -y install sshpass rsync doxygen nodejs npm

WORKDIR /usr/local
RUN npm install docusaurus

RUN mkdir /root/.ssh
COPY ./id_rsa /root/.ssh/
RUN chmod 700 /root/.ssh && chmod 600 /root/.ssh/*

COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh

ENTRYPOINT [ "/entrypoint.sh" ]