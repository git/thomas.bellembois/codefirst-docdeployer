#!/usr/bin/env bash

echo "Repository name: "$DRONE_REPO_NAME
echo "Repository owner: "$DRONE_REPO_OWNER

echo "NODOXYGEN: "$NODOXYGEN
echo "NODUSAURUS: "$NODUSAURUS
echo "NOSWAGGER: "$NOSWAGGER

echo "Generating and deploying documentation for user $DRONE_REPO_OWNER and repository $DRONE_REPO_NAME"

# /docs must be declared as a temporary volume in drone.yml
mkdir -p /docs/doxygen/
mkdir -p /docs/docusaurus/

# Check documentation directory syntax.
DOCDIR="Documentation"
[ -d "/drone/src/documentation" ] && DOCDIR="documentation" 

# Generate doxygen.
if [[ -z "${NODOXYGEN}" ]]
then 
  echo "- generating doxygen"

  [ -d "/drone/src/$DOCDIR/doxygen" ] && cd /drone/src/$DOCDIR/doxygen && doxygen Doxyfile
else
  # Create empty directory for rsync to delete remote content.
  rm -Rf /docs/doxygen/
  mkdir -p /docs/doxygen/
fi

# Generate docusaurus.
if [[ -z "${NODOCUSAURUS}" ]]
then 
  echo "- generating docusaurus"

  [ -d "/drone/src/$DOCDIR/docusaurus" ] && cd /drone/src/$DOCDIR/docusaurus && npm install && npm run build
else
  # Create empty directory for rsync to delete remote content.
  rm -Rf /drone/src/$DOCDIR/docusaurus/build/
  mkdir -p /drone/src/$DOCDIR/docusaurus/build/
fi

# Generate swagger CLICKME file.
if [[ -z "${NOSWAGGER}" ]]
then
  echo "- generating swagger"
 
  swaggerfile=$(find /docs -maxdepth 1 -type f -iname "swagger.*" -printf "%f" -quit)
  if [[ ! -z "$swaggerfile" ]]
  then
    echo "<!DOCTYPE html>
            <html>
              <head>
                <meta http-equiv="Refresh" content=\"0; url=/swagger?url=/documentation/$DRONE_REPO_OWNER/swagger/$DRONE_REPO_NAME/$swaggerfile\" />
              </head>
              <body>
                <p>Suivez <a href=\"/swagger?url=/swagger?url=/documentation/$DRONE_REPO_OWNER/swagger/$DRONE_REPO_NAME/$swaggerfile\">ce lien</a>.</p>
              </body>
            </html>" > /docs/CLICKME.html

    echo "- pushing swagger on remote"

    rsync --rsync-path="mkdir -p /usr/share/nginx/html/$DRONE_REPO_OWNER/swagger/$DRONE_REPO_NAME/ && rsync" -e "ssh -o StrictHostKeyChecking=no" -avz --delete /docs/{swagger.*,CLICKME.html} root@nginx:/usr/share/nginx/html/$DRONE_REPO_OWNER/swagger/$DRONE_REPO_NAME/
  else
    echo "- deleting swagger from remote"

    mkdir /docs/empty/
    rsync --rsync-path="mkdir -p /usr/share/nginx/html/$DRONE_REPO_OWNER/swagger/$DRONE_REPO_NAME/ && rsync" -e "ssh -o StrictHostKeyChecking=no" -avz --delete /docs/empty/ root@nginx:/usr/share/nginx/html/$DRONE_REPO_OWNER/swagger/$DRONE_REPO_NAME/
  fi
  
fi

cd /drone/src/$DOCDIR

echo "- rsync doxygen with remote"

[ -d "/drone/src/$DOCDIR/doxygen" ] && rsync --rsync-path="mkdir -p /usr/share/nginx/html/$DRONE_REPO_OWNER/doxygen/$DRONE_REPO_NAME/ && rsync" -e "ssh -o StrictHostKeyChecking=no" -avz --delete /docs/doxygen/ root@nginx:/usr/share/nginx/html/$DRONE_REPO_OWNER/doxygen/$DRONE_REPO_NAME/

echo "- rsync docusaurus with remote"

[ -d "/drone/src/$DOCDIR/docusaurus" ] && rsync --rsync-path="mkdir -p /usr/share/nginx/html/$DRONE_REPO_OWNER/docusaurus/$DRONE_REPO_NAME/ && rsync" -e "ssh -o StrictHostKeyChecking=no" -avz --delete docusaurus/build/ root@nginx:/usr/share/nginx/html/$DRONE_REPO_OWNER/docusaurus/$DRONE_REPO_NAME/

exit 0